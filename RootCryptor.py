#################################################################################################################
# Name: RootCryptor2
# Author: Faheem (Mr Root)
# Description: this is an Encryptor and Decryptor that is capable of quantum-proof encryption like crystal kyber
# Other encryption like AES and Caesar encryption.all encryption takes a password or a key and data.
#################################################################################################################

from main.Navigator.Navigator import NavigatorClass

class RootCryptorClass:
    '''this is a console base implementation of the RootCryptor this allows you to encrypt the data or the string that you want to encrypt'''

    def Main(self) -> None:
        '''this is where the actual UI is called'''

        UI = NavigatorClass()
        UI.MainUI()
        return None

rootCryptor = RootCryptorClass()
rootCryptor.Main()

'''
try:
    rootCryptor.Main()
except:
    input("Sorry Something Went Wrong,Press Enter To Continue .....................")
        
 
'''