import pyfiglet
#big
banner = pyfiglet.figlet_format("Hello, world!","chunky")

print(banner)

#small
banner = pyfiglet.figlet_format("Hello, world!","threepoint")

print(banner)