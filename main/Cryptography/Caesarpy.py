#################################################################################################################
# Name: Caesar Cypher
# Author: Faheem (Mr Root)
# Description: This is a Basic Encoder decoder used to encrypt and decrypt the text
# Working: This is going to have 2 methods One is to encrypt which with adding the cipher value another is
#          decryptor that reduces the value and gives the character the character limit is only 33 to 126
#################################################################################################################

import math


class CaesarClass:
    """This is the implementation of the Caesar cipher This has 3 methods Caesar(), Encryptor(,) and Decryprtor(,)"""

    def Caesar(self,data: str, key: int, encryption: bool) -> str:
        """Caesar(password str, key int, encryption_status bool) -> returns Encrypted (or) Decrypted str
        \nwhere password is the string that needs to be encrypted or decrypted
        \nkey is the value of the Caesar shifts
        \nencryprtion_status -> ( true: encrypt [or] false: decrypt ) This shows the operation to be performed
        \nThis only supports ASCII standard characters from 33 to 126"""

        outputText: str= ""
        if not encryption:
            key = -key

        for character in data:
            # This is where the shifting takes place
            outputText = outputText + CaesarClass.Cypher(character, key)

        return outputText

    def Cypher(character: str, key: int) -> str:
        """Cypther(character str, key int) -> returns Encrypted or Decrypted character str
        \nonly one character should be passed this is where the range is validated between 33 to 126"""
        outputCharacter = ord(character) + key
        
        while outputCharacter < 33:
            outputCharacter = outputCharacter - 33 + 126

        while outputCharacter > 126:
            outputCharacter = outputCharacter - 93

        return chr(outputCharacter)
