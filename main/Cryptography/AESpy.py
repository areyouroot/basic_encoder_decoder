#################################################################################################################
# Name: AES 256 in root crypt
# Author: Faheem (Mr Root)
# Description: This is a Basic AES 256 encryptor and decryptor that uses MD5 hash as key and nonce
# Working: this is an AES Encryptor that takes a password from the user as a data or string then encrypts the data
#################################################################################################################

from Crypto.Cipher import AES
import hashlib

class RootAESClass:
    '''this class is used to do the AES Encryption and Decryption
    where the data that needs to be encrypted and the password data is 
    passed and cryptography is performed and the type of AES used is
    AES.MODE_EAX'''

    key: bytes
    nonce: bytes

    def AesEncryptor(self,data: bytes, password: bytes) -> bytes:
        '''AesEncryptor(data bytes,password bytes) -> returns encryptedData bytes
        \nthis is a basic method where AES Encryption takes place
        \nWhere :
        \ndata is a byte that contains the secret text.
        \nPassword is a byte, or password which is used to encrypt the message.
        \nthe return type is an Encrypted message that is in the byte format.'''

        key, nonce = self.HashConvetor(password)
        cipher = AES.new(key=key,mode=AES.MODE_EAX)
        cipherData = cipher.encrypt(data)
        return cipherData

    def AesDecryptor(self,encryptedData :bytes, password: bytes) -> bytes:
        '''AesDecryptor(encryptedData bytes, password bytes) -> returns decryptedData str
        \nthis is where the decryption happens
        \nhere:
        \nEncryptedData is an Encrypted byte that is going to be decrypted.
        \nPassword is a byte that is used as a password to decrypt.
        \nthe return type is a Decrypted data that is in the data format'''

        key, nonce = self.HashConvetor(password)
        cipher = AES.new(key=key,mode=AES.MODE_EAX)
        data = cipher.decrypt(encryptedData)
        return data


    def HashConvetor(self,value: bytes) -> bytes:
        '''HashConvetor(value bytes) -> returns Hash bytes
        \nthis is a method used to convert the plain text into md5 hash'''

        key = hashlib.md5()
        nonce = hashlib.md5(key.digest())
        return key.digest(),nonce.digest()
    