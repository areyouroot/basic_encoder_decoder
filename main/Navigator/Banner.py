#################################################################################################################
# Name: Banner
# Author: Faheem (Mr Root)
# Description: this is a banner and a banner generator for console UI
#################################################################################################################

import pyfiglet
import pyfiglet.fonts
import os

class BannerClass:
    '''this is the class for the banner functions'''

    def Title(self) -> None:
        '''this is a banner showing Tittle'''

        try:
            banner = pyfiglet.figlet_format("Root Cryptor","chunky")
            print(banner)
        except :
            print("Root Cryptor")
        finally:
            return None

    def SubTittle(self,string: str) -> None:
        """SubTittle(tittle str) -> None this is a banner for small or sub title this has print function that prints the tittle"""

        try:
            banner = pyfiglet.figlet_format(string,"threepoint")
            print(banner)
        except :
            print(string)
        finally:
            return None
        
    def ClearConsole(self) -> None:
        """this is a function to clear the console"""
        
        try:
            if os.name == 'nt':
                os.system("cls")
                
            else:
                os.system("clear")            
            
        except:
            pass