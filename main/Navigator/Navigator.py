#################################################################################################################
# Name: Navigator
# Author: Faheem (Mr Root)
# Description: this is the UI navigator to use the RootCryptor this is where the functional UI is defined
#################################################################################################################

from main.Navigator.Banner import BannerClass
from main.Operations.CaesarUI import CaesarUIClass
from main.Operations.RootAESUI import RootAESUI

banner = BannerClass()
caesarUI = CaesarUIClass()
rootAES = RootAESUI()

class NavigatorClass:
    '''this is a main class of navigator for ui implementation'''
    
    def MainUI(self):
        '''this is the main function where the Entire UI is called'''

        self.MainMenu()

    def MainMenu(self):
        '''this the main menu'''
        while True:
            banner.ClearConsole()
            banner.Title()
            print("Welcome to Root Cryptor")
            print("Select the Encrytpion option")
            print("1. Caesar")
            print("2. AES")
            print("3. PQE")
            print("4. Exit")
            choice = int(input("Enter Your Choice: "))
            
            if (choice == 1):
                caesarUI.MainMenu()
            
            elif (choice == 2):
                rootAES.Main()
                
            elif (choice == 3):
                input("this Feature is Under Developement Please Do check Later \npress Enter to Continue .........")
                
            else:
                input("press Enter to continue .......")
                break
            
            


