#################################################################################################################
# Name: Navigator
# Author: Faheem (Mr Root)
# Description: this is the UI navigator to use the RootCryptor this is where the functional UI is defined this
# is for the AES encryption
#################################################################################################################

from logging import root
from main.Cryptography.AESpy import RootAESClass
from main.Navigator.Banner import BannerClass

banner = BannerClass()
rootAES = RootAESClass()

class RootAESUI:
    '''this is a class where the aes console based ui is given'''
    
    def Main(self):
        '''this is the main ui implementation of the AES'''
        
        banner.ClearConsole()
        banner.SubTittle("Welcome To ROOT's AES")
        print("1. Encrytpion")
        print("2. Decreption")
        print("3. Go Back")
        choice = int(input("Enter Your Choice: "))
        
        if (choice == 1):
            data:str = input("Enter the data for the Encryption: ")     
            password:str = input("Enter The password: ")
            dataBytes:bytes = bytes(data,'ascii') 
            print(dataBytes)
            print(dataBytes.hex())
            passwordBytes:bytes = bytes(password,'ascii')
            encryptedData = rootAES.AesEncryptor(dataBytes,passwordBytes)
            print("The Ecrypted Data: ")
            print(encryptedData.hex())

            #

            dataBytes = bytes.fromhex(encryptedData.hex())
            actuallData = rootAES.AesDecryptor(dataBytes,passwordBytes)
            print("The Decrepted Data: ")
            print(actuallData)
            test = actuallData.hex()
            test = bytes.fromhex(test)
            test = test.decode('utf-8')
            print(test)
        
        elif(choice == 2):
            dataHex:int = input("Enter the Encrypted Data: ")
            password:str = input("Enter The password: ")
            passwordBytes:bytes = bytes(password,'ascii')
            dataBytes = bytes.fromhex(dataHex)
            actuallData = rootAES.AesDecryptor(dataBytes,passwordBytes)
            print("The Decrepted Data: ")
            test = actuallData.hex()
            test = bytes.fromhex(test)
            test = test.decode('utf-8')
            print()
            print()
            
        input("Press Enter to continue ..............")