#################################################################################################################
# Name: Navigator
# Author: Faheem (Mr Root)
# Description: this is the UI navigator to use the RootCryptor this is where the functional UI is defined this
# is for the Caesar encryption
#################################################################################################################

from time import sleep
from main.Navigator.Banner import BannerClass
from main.Cryptography.Caesarpy import CaesarClass

banner = BannerClass()
caesar = CaesarClass()

class CaesarUIClass:
    """this is the console based Caesar encryption"""
    
    def MainMenu(self) -> None:
        '''this is the main UI'''
        
        banner.ClearConsole()
        banner.SubTittle("Caesar Cypher")
        print("Welcome to Caesar,rome was not built in a day")
        print("1. Encryption")
        print("2. Decryption")
        print("3. Go Back")
        choice = int(input("Enter Your Choice : "))
        print("Eu tu Brute")
        sleep(2)
        if choice == 1 or choice == 2:
            self.Cypher(choice)
    
    def Cypher(self,choice: int) -> None:
        '''this is where the Class is used for encryption'''
        
        data = input("Enter Your Secret:")
        try:
            key = int(input("Enter the Secret key:"))
        except:
            input("Enter a vaild number please press Enter to continue ...........")
            self.Cypher(choice)
            
        if choice == 1:
            data = caesar.Caesar(data,key,True)
        
        if choice == 2:
            data = caesar.Caesar(data,key,False)
         
        print("The Encrypted Data is "+data)
        input("Press Enter t0 Continue..............")
        
        
    