#################################################################################################################
# Name: Caesar Cypher CT
# Author: Faheem (Mr Root)
# Description: This is a Basic Encoder decoder used to encrypt and decrypt the text
# Working: This is going to have 2 methods One is to encrypt which with adding the cipher value another is
#          decryptor that reduces the value and gives the character the character limit is only 33 to 126
#################################################################################################################

import unittest
import sys
import string
import random

sys.path.insert(0,'Cryptography');
import Caesarpy as Cs

class CaesarTestClass(unittest.TestCase):
    def test(self):
        '''this test case compares the actual string and the encrypted and decrepted string
         if both are true the test passes else fail '''
        cs1 = Cs.CaesarClass
        length = int(random.random() *100) + 1
        word = ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

        #basic encoding testing
        encrypted = cs1.Caesar(word, 333, True)
        decripted = cs1.Caesar(encrypted, 333, False)
        self.assertNotEqual(word,encrypted)#encryption
        self.assertEqual(word,decripted)#decription

        #testing limits
        negativeEncrypted = cs1.Caesar(word, -333, True)
        negativeDecrypted = cs1.Caesar(negativeEncrypted, -333, False)
        self.assertNotEqual(word,negativeEncrypted)
        self.assertEqual(word,negativeDecrypted)


if __name__ == "__main___":
    unittest.main()
